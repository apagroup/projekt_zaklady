<?php

session_start();
 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index.php");
    exit;
}
 
$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
				if($db===false){
					die("Error:".mysqli_connect_error());
				}

$username = $password = "";
$username_err = $password_err = "";
 

if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    
    if(empty(trim($_POST["username"]))){
        $username_err = "Podaj nazwę użytkownika";
    } else{
        $username = trim($_POST["username"]);
    }
    
   
    if(empty(trim($_POST["password"]))){
        $password_err = "Podaj hasło";
    } else{
        $password = trim($_POST["password"]);
    }
    
   
    if(empty($username_err) && empty($password_err)){
        
        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($db, $sql)){
            
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            
            $param_username = $username;
            
            
            if(mysqli_stmt_execute($stmt)){
            
                mysqli_stmt_store_result($stmt);
                
                
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            
                            session_start();
                            
                            
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            
                            header("location: index.php");
                        } else{
                           
                            $password_err = "Hasło nieprawidłowe";
                        }
                    }
                } else{
                    
                    $username_err = "Nie ma takiego konta!";
                }
            } else{
                echo "Błąd... Coś poszło nie tak.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    
    mysqli_close($db);
}
?>
 
<!DOCTYPE html>
<html lang="pl">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Zakłady Typowy Typer</title>

  <!-- Font Awesome Icons -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index1.html">Zakłady Typowy Typer</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="register.php">Załóż konto</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#page-top">Zaloguj się</a>
			
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <header class="masthead">
    <div>
	    <div class="text-white-75 font-weight-light mb-5" style="margin-top: 150px;">
	    
	    <center>
        <h2>Logowanie do serwisu</h2>
		<br>
		<div class="row align-items-center justify-content-center">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">	
            <div <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>>
			  <div class="input-group mb-4">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1" style="width: 200px;" >Nazwa użytkownika : </span>
              </div>
               <input type="text" class="form-control" placeholder="<?php echo $username_err; ?> " name="username" style="width: 230px;" value="<?php echo $username; ?>">
                </div>
				 
            </div>  
			</div>
			
			<div class="row align-items-center justify-content-center">
			
            <div <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>>
                <div class="input-group mb-4">
                <div class="input-group-prepend">
			
               <span class="input-group-text" id="basic-addon1" style="width: 200px;" >Hasło :</span>
			   
              </div>
                <input type="password" class="form-control" style="width: 230px;" placeholder="<?php echo $password_err; ?>" name="password" >
                <span></span>
            </div>
            <div>
			     <button type="sumbit" value="Login" class="btn btn-primary btn-xl js-scroll-trigger">Zaloguj się</button>
                
            </div>
			
            <br><br><br><p><b>Nie masz u nas konta? </b><a href="register.php" ><b>Zarejestruj się!</b></a></p></br>
			</div>
			</div>
			</center>
			</form>
		</div>
    </div>  
</header>
<footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">Copyright &copy; 2019 - Zakłady Typowy Typer</div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/creative.min.js"></script>
</body>

</html>