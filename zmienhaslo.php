<?php
	include "navbar-menu.php";
	echo "<header class='masthead'>";
		$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
		if($db===false){
			die("Error:".mysqli_connect_error());
		}
		
		$old_password = $new_password = $confim = "";
		$old_err = $new_err = $confim_err = $komunikat = "";
		
	if($_SERVER["REQUEST_METHOD"]== "POST"){
		
		if(empty(trim($_POST["old"])))
				$old_err = "Podaj hasło";     
			else
				$old_password = trim($_POST["old"]);
			
			
			if(empty(trim($_POST["new"]))){
				$new_err = "Podaj hasło.";     
			} elseif(strlen(trim($_POST["new"])) < 6){
				$new = "Hasło musi posiadać conajmniej 6 znaków!";
			} else{
				$new_password = trim($_POST["new"]);
			}
			
			if(empty(trim($_POST["confim"]))){
				$confim_err = "Podaj ponownie hasło";     
			} else{
				$confim = trim($_POST["confim"]);
				if(empty($new_err) && ($new_password != $confim)){
					$confim_err = "Hasła różnią się";
				}
			}
			
		
		if(empty($old_err) && empty($new_err) && empty($confim_err)){
			
			$zapyt = "SELECT id,username,password FROM users WHERE username = ?";
			if($stmt = mysqli_prepare($db, $zapyt)){
				
				mysqli_stmt_bind_param($stmt,"s",$param_username);
				
				$param_username = $_SESSION["username"];
				
				if(mysqli_stmt_execute($stmt)){
            
					mysqli_stmt_store_result($stmt);
					
					if(mysqli_stmt_num_rows($stmt) == 1){                    
                    
						mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
						if(mysqli_stmt_fetch($stmt)){
							if(password_verify($old_password, $hashed_password)){
								
								$zmiana = "UPDATE users SET password = ? WHERE username = ?";
								if($stmt = mysqli_prepare($db,$zmiana)){
									
									mysqli_stmt_bind_param($stmt,"ss",$param_new,$param_username);
									$param_new = password_hash($new_password,PASSWORD_DEFAULT);
									$param_username = $_SESSION["username"];
									if(mysqli_stmt_execute($stmt)){
										$komunikat = "<font class='text-warning'><b>Haslo zmienione!</b></font>";
									}else
										$komunikat = "<font class='text-warning'><b>Błąd!</b></font>";
								}			
								
							}else 
								$old_err="<font class='text-warning'><b>Błędne hasło!</b></font>";	
						}        
					}
				}
			}
			
		}
		mysqli_close($db);	
	}		
	?>	
	<h3 class='text-white row align-items-center justify-content-center'>Zmien hasło</h3>
	<div class="row align-items-center justify-content-center" style="margin-top: 10px;">
	<form action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="POST">
	<div class="input-group mb-4">
    <div class="input-group-prepend">
	<span class="input-group-text " id="basic-addon1"style="width: 200px;" >Stare hasło:</span>
	</div>
	<input type="password" name="old" style="width: 230px;" placeholder="<?php echo $old_err; ?>">
	</div>
	
	<div class="input-group mb-4">
    <div class="input-group-prepend">
	<span class="input-group-text" id="basic-addon1" style="width: 200px;">Nowe hasło:</span>
	</div>
	<input type="password" name="new" style="width: 230px;" placeholder="<?php echo $new_err; ?>">
	</div>
	
	<div class="input-group mb-4">
		<div class="input-group-prepend">
			<span class="input-group-text" id="basic-addon1" style="width: 200px;">Powtórz nowe hasło:</span>
		</div>
		<input type="password" name="confim" style="width: 230px;" placeholder="<?php echo $confim_err; ?>">
	</div>
	
	
	<div class="row align-items-center justify-content-center">
		<button type="sumbit" value="Zmień" class="btn btn-primary btn-xl js-scroll-trigger">Zmień</button>
	</div>
	
	<div class="row align-items-center justify-content-center">
		<span><?php echo $komunikat; ?></span>
	</div>
	</form>

	<?php 
		echo "</header>";
		include "footer.php";
	?>