﻿
<?php
	include "navbar-menu.php";
	echo "<header class='masthead'>";
	$kwota = $error = $komunikat = "";
	
	$db=mysqli_connect("localhost", "root","","projektzaklady");
				if($db===false){
					die("Error:".mysqli_connect_error());
				}
	
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		echo $_POST['kwota'];
		if(empty($_POST["kwota"]))
			$error = "<font class='text-warning'><b>Podaj kwotę!</b></font>";
		else if(!is_numeric($_POST["kwota"]))
			$error = "<font class='text-warning'><b>Zły format kwoty!</b></font>";
		else if($_POST["kwota"] > 100)
			$error = "<font class='text-warning'><b>Nie możesz jednorazowo wpłacić więcej niż 100 PLN</b></font>";
		else if($_POST["kwota"]<=0)
			$error = "<font class='text-warning'><b>Podaj kwotę!</b></font>";
		else
			$kwota = $_POST["kwota"];
		
		
			if(empty($error))
			{
				$sql = "UPDATE users SET konto = konto + ? WHERE username = ?";
				
				if($stmt = mysqli_prepare($db,$sql))
				{					
					mysqli_stmt_bind_param($stmt,"ds",$param_kwota,$param_username);
					$param_kwota = $kwota;
					$param_username = $_SESSION["username"];
					
					if(mysqli_stmt_execute($stmt)){
						
						header("location: zatw_plat.php");
					}	
					else
						$komunikat =  "<font class='text-warning'><b>Błąd</b></font>";
				}
				
			}
		
		
	}	

?>
<h3 class='text-white row align-items-center justify-content-center'>Płatności</h3>
    <div class="row align-items-center justify-content-center" style="margin-top: 50px;">
	<form action="platnosci.php" method="POST">
	
	<div class="input-group mb-4">
		<div class="input-group-prepend">
			<span class="input-group-text" id="basic-addon1" >Kwota:</span>
		</div>
		<input type="text" name="kwota" placeholder="0.00 PLN">
	</div>
		
	<div class="row align-items-center justify-content-center">
		<button type="sumbit" value="Wpłać" class="btn btn-primary btn-xl js-scroll-trigger">Wpłać</button>
	</div>
		
		
	</form>
	</div>
	<span class='row align-items-center justify-content-center'style ="color:white"><?php echo $error; echo $komunikat; ?></span>
<?php
echo "</header>";
include "footer.php";

?>