<!doctype html>

<html>
	<head>
	<!-- Font Awesome Icons -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.min.css" rel="stylesheet">
	
		<title>Typowy typer</title>
	</head>
	
	  
	  <body id="page-top">
	  <?php
	  session_start();
			if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
				header("location: login.php");
			exit;
			}
			
		$db=mysqli_connect("localhost", "root","","projektzaklady");
				if($db===false){
					die("Error:".mysqli_connect_error());
				}
		
			
						$uzyt = $_SESSION["username"];
						$zapyt = "SELECT konto FROM `users` WHERE username = '$uzyt'";
						$stan = mysqli_query($db,$zapyt);
						$wynik = mysqli_fetch_row($stan);
						$asd = $wynik[0];
						
			
		?>
			
	  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php">Zakłady Typowy Typer</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto my-2 my-lg-0">
					<!--
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="wyniki.php">Wyświetlanie meczów</a> 
					</li>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="tabela.php">Tabela</a>
					</li>-->
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger text-warning" href="obstawianie.php">Postaw Kupon</a>
					</li>
					
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="pilka_nozna" href="#">Piłka Nożna
						</a> 
						<div class="dropdown-menu" aria_labelledby="pilka_nozna">
							<a class="dropdown-item" href="tabela.php">Tabela</a>
							<a class="dropdown-item" href="wyniki.php">Wyniki Meczów</a>
						</div>
					</li>

					<?php if($_SESSION['id']==1): ?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="admin_panel" href="#">Panel Administratora</a> 
						<div class="dropdown-menu" aria_labelledby="admin_panel">
                 <a class="dropdown-item" href="form.php">Dodaj Mecz</a>
                <a class="dropdown-item" href="admin_dodaj_wynik.php">Wpisz Wyniki</a>
                <a class="dropdown-item" onClick='alert("Tabela została zaktualizowana!")' href="aktualizacjaTab.php">Aktualizuj Tabele</a>
                <a class="dropdown-item" onClick='alert("Aby zatwierdzić kupony najpierw musisz zaktualizować tabelę! Kupony zostały zatwierdzone!")' href="zatw_kuponow.php">Zatwierdź kupony</a>
						</div>
					</li>
					<?php endif; ?>

					<li class="nav-item">
						<a class="nav-link js-scroll-trigger">Portfel: <?php echo $asd." PLN";?></a>
					</li>

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="user" href="#"><?php echo " ". htmlspecialchars($_SESSION["username"]); ?></a> 
						<div class="dropdown-menu" aria_labelledby="user">
							<a class="dropdown-item" href="statystyki.php">Moje Kupony</a>
							<a class="dropdown-item" href="platnosci.php">Płatności</a>
							<a class="dropdown-item" href="pobierz_wygrana.php">Pobierz wygraną</a>
							<a class="dropdown-item" href="zmienmail.php">Zmiana Maila</a>
                            <a class="dropdown-item" href="zmienhaslo.php">Zmiana Hasła</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="logout.php">Wyloguj</a>
						</div>
					</li>
		   
        </ul>
      </div>
		</div>
		</div>
	</nav>
