<?php include "navbar-menu.php";?>
<header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Typowy Typer</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">Analizuj, obstawiaj i zwyciężaj!</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#jak_grac">JAK GRAĆ</a>
        </div>
      </div>
    </div>
  </header>
	  
	
	 
<section class="page-section bg-dark" id="jak_grac">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-20 text-center">
            <h1 class="text-white mt-0">Jak grać?</h1>
            <hr class="divider my-4">
            <h3 class="text-white">Depozyt</h3>
            <h5 class="text-white">Przejdź do zakładki Płatności, która jest zlokalizowana na górze strony przez kliknięcie na nazwę użytkownika. 
              Wybierz preferowaną metodę płatności.</h5>
            <hr class="divider my-4">
            <h3 class="text-white">Postaw Kupon</h3>
            <h5 class="text-white">Teraz czas na zabawę. Stawianie zakładu! 
              Możesz wybrać interesujące Cię mecze oraz postawić na wybrane drużyny stawiając dowolną sumę pieniędzy.
              Stawka odnosi się do kwoty za którą chcesz postawić kupon.</h5>
            <hr class="divider my-4">
            <h3 class="text-white">Moje Kupony</h3>
            <h5 class="text-white">Bardzo prosto możesz sprawdzić wszystkie postawione zakłady. 
              Po prostu przejdź do zakładki Moje kupony, która jest dostępna po kliknięciu na swoją nazwę użytkownika. 
              Możesz tu sprawdzić historię zakładów, razem z wciąż trwającymi i/lub zakończonymi.</h5>
              <hr class="divider my-4">
          </div>
        </div>
      </div>
    </section>
	
<!-- Contact Section -->
	<section class="page-section" id="contact">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="mt-0">Kontakt</h2>
          <hr class="divider my-4">
          <p class="text-muted mb-5">
						Jeśli masz pytania to służymy pomocą. Nasza infolinia jest czynna od Pon do Pt w godzinach 8:00 do 20:00 lub zapraszamy na nasz adres email, który jest dostępny całą dobę.
					</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center text-primary">
          <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
          <div>+48  535-485-787</div>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
          <!-- Make sure to change the email address in anchor text AND the link below! -->
          <a class="d-block" href="mailto:contact@yourwebsite.com">typowytyper@biuro.com</a>
        </div>
      </div>
    </div>
  </section>
	
<?php echo "</header>";
	include "footer.php";?>