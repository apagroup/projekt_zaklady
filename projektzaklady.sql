-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 09 Cze 2019, 19:50
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projektzaklady`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kupony`
--

CREATE TABLE `kupony` (
  `id_kuponu` int(11) NOT NULL,
  `id_uzytkownika` int(11) NOT NULL,
  `postawiona_suma` decimal(10,2) NOT NULL,
  `wygrana_suma` decimal(10,2) NOT NULL,
  `status` text NOT NULL,
  `status_zap` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `kupony`
--

INSERT INTO `kupony` (`id_kuponu`, `id_uzytkownika`, `postawiona_suma`, `wygrana_suma`, `status`, `status_zap`) VALUES
(1, 1, '20.00', '0.00', 'Oczekuje', 0),
(2, 1, '12.00', '0.00', 'Przegrana', 0),
(3, 1, '30.00', '0.00', 'Przegrana', 0),
(4, 2, '100.00', '200.00', 'Wygrana', 1),
(5, 2, '50.00', '0.00', 'Oczekuje', 0),
(6, 3, '4.00', '0.00', 'Oczekuje', 0),
(7, 1, '4.00', '0.00', 'Oczekuje', 0),
(8, 4, '20.00', '0.00', 'Oczekuje', 0),
(9, 1, '2.00', '4.40', 'Oczekuje', 0),
(10, 1, '2.00', '4.50', 'Wygrana', 1),
(11, 1, '2.00', '3.00', 'Wygrana', 1),
(12, 1, '2.00', '0.00', 'Wygrana', 1),
(14, 1, '2.00', '0.00', 'Wygrana', 1),
(15, 1, '80.50', '0.00', 'Oczekuje', 0),
(16, 1, '80.00', '0.00', 'Wygrana', 1),
(17, 1, '12.00', '0.00', 'Oczekuje', 0),
(18, 1, '11.00', '0.00', 'Oczekuje', 0),
(19, 1, '20.00', '0.00', 'Przegrana', 0),
(20, 1, '11.00', '31.90', 'Przegrana', 0),
(21, 1, '20.00', '0.00', 'Wygrana', 1),
(22, 1, '80.00', '0.00', 'Przegrana', 0),
(23, 1, '25.50', '0.00', 'Wygrana', 1),
(24, 1, '25.00', '45.00', 'Przegrana', 0),
(25, 1, '18.00', '0.00', 'Wygrana', 1),
(26, 1, '20.00', '26.00', 'Przegrana', 0),
(27, 1, '10.00', '0.00', 'Wygrana', 1),
(28, 1, '10.00', '0.00', 'Wygrana', 1),
(29, 1, '2.00', '0.00', 'Wygrana', 1),
(30, 1, '20.00', '0.00', 'Przegrana', 0),
(31, 1, '11.00', '0.00', 'Wygrana', 1),
(32, 1, '2.66', '0.00', 'Wygrana', 1),
(33, 1, '2.66', '3.99', 'Wygrana', 1),
(34, 1, '22.00', '59.40', 'Przegrana', 0),
(35, 1, '22.50', '33.75', 'Wygrana', 1),
(36, 1, '25.50', '0.00', 'Przegrana', 0),
(37, 1, '50.00', '0.00', 'Przegrana', 0),
(38, 1, '50.00', '0.00', 'Przegrana', 0),
(39, 1, '77.00', '0.00', 'Przegrana', 0),
(40, 1, '11.00', '50.60', 'Wygrana', 1),
(41, 1, '15.00', '21.00', 'Wygrana', 1),
(42, 1, '12.00', '0.00', 'Przegrana', 0),
(43, 1, '10.00', '43.00', 'Przegrana', 0),
(44, 2, '11.00', '0.00', 'Przegrana', 0),
(45, 2, '20.00', '86.00', 'Przegrana', 0),
(46, 2, '11.00', '0.00', 'Przegrana', 0),
(47, 2, '11.00', '47.30', 'Przegrana', 0),
(48, 2, '11.00', '15.40', 'Wygrana', 1),
(49, 2, '12.00', '0.00', 'Przegrana', 0),
(50, 2, '12.00', '16.80', 'Wygrana', 1),
(51, 2, '11.00', '0.00', 'Przegrana', 0),
(52, 2, '11.00', '0.00', 'Przegrana', 0),
(53, 2, '12.00', '0.00', 'Przegrana', 0),
(54, 2, '20.00', '0.00', 'Przegrana', 0),
(55, 2, '12.00', '0.00', 'Przegrana', 0),
(56, 2, '5.00', '0.00', 'Przegrana', 0),
(57, 2, '5.00', '0.00', 'Przegrana', 0),
(58, 2, '5.00', '0.00', 'Przegrana', 0),
(59, 2, '10.00', '34.00', 'Przegrana', 0),
(60, 2, '10.00', '34.00', 'Przegrana', 0),
(61, 2, '10.00', '34.00', 'Przegrana', 0),
(62, 2, '11.10', '37.74', 'Przegrana', 0),
(63, 2, '12.66', '17.72', 'Wygrana', 1),
(64, 2, '11.88', '16.63', 'Wygrana', 1),
(65, 2, '11.88', '16.63', 'Wygrana', 1),
(66, 1, '222.00', '310.80', 'Wygrana', 1),
(67, 1, '222.00', '310.80', 'Wygrana', 1),
(68, 1, '20.00', '0.00', 'Przegrana', 0),
(69, 1, '3.00', '20.40', 'Przegrana', 0),
(70, 1, '2.00', '3.40', 'Przegrana', 0),
(71, 1, '11.00', '71.50', 'Przegrana', 0),
(72, 1, '12.00', '124.80', 'Wygrana', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mecze`
--

CREATE TABLE `mecze` (
  `id` int(11) NOT NULL,
  `gospodarz` text NOT NULL,
  `gosc` text NOT NULL,
  `bramkiGospodarz` varchar(3) DEFAULT NULL,
  `bramkiGosc` varchar(3) DEFAULT NULL,
  `dataSpotkania` date NOT NULL,
  `godzinaSpotkania` time NOT NULL,
  `wpisany_wynik` tinyint(1) NOT NULL,
  `czy_aktualizowana` tinyint(1) NOT NULL,
  `kurs_gospodarz` float(2,1) NOT NULL,
  `kurs_gosc` float(2,1) NOT NULL,
  `kurs_remis` float(2,1) NOT NULL,
  `kto_wygral` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `mecze`
--

INSERT INTO `mecze` (`id`, `gospodarz`, `gosc`, `bramkiGospodarz`, `bramkiGosc`, `dataSpotkania`, `godzinaSpotkania`, `wpisany_wynik`, `czy_aktualizowana`, `kurs_gospodarz`, `kurs_gosc`, `kurs_remis`, `kto_wygral`) VALUES
(1, 'Legia Warszawa', 'Lech Poznan', '2', '2', '2019-03-21', '21:14:00', 1, 1, 0.0, 0.0, 0.0, ''),
(2, 'Gornik Zabrze', 'Piast Gliwice', '3', '1', '2019-03-22', '11:11:00', 1, 1, 0.0, 0.0, 0.0, ''),
(3, 'Arka Gdynia', 'Wisla Krakow', '1', '2', '2019-03-27', '15:45:00', 1, 1, 0.0, 0.0, 0.0, ''),
(4, 'Wisla Krakow', 'Lech Poznan', '5', '3', '2019-03-18', '20:30:00', 1, 1, 0.0, 0.0, 0.0, ''),
(5, 'Lechia Gdansk', 'Gornik Zabrze', '1', '2', '2019-03-15', '20:45:00', 1, 1, 0.0, 0.0, 0.0, ''),
(6, 'Wisla Krakow', 'Legia Warszawa', '1', '2', '2019-04-13', '23:41:00', 1, 1, 2.2, 1.5, 1.8, 'Legia Warszawa'),
(7, 'Lech Poznan', 'Piast Gliwice', '3', '2', '2019-04-13', '15:15:00', 1, 1, 1.5, 2.5, 2.1, 'Lech Poznan'),
(8, 'Lechia Gdansk', 'Arka Gdynia', '3', '4', '2019-04-13', '05:54:00', 1, 1, 0.0, 0.0, 0.0, ''),
(9, 'Piast Gliwice', 'Legia Warszawa', '3', '2', '2019-04-12', '20:45:00', 1, 1, 0.0, 0.0, 0.0, 'Piast Gliwice'),
(10, 'Lech Poznan', 'Arka Gdynia', '3', '1', '2019-04-12', '15:30:00', 1, 1, 0.0, 0.0, 0.0, ''),
(11, 'Gornik Zabrze', 'Wisla Krakow', '0', '0', '2019-04-14', '18:20:00', 1, 1, 0.0, 0.0, 0.0, 'remis'),
(12, 'Legia Warszawa', 'Arka Gdynia', '1', '0', '2019-04-14', '11:11:00', 1, 1, 0.0, 0.0, 0.0, 'Legia Warszawa'),
(13, 'Lechia Gdansk', 'Arka Gdynia', '2', '1', '2019-04-13', '06:06:00', 1, 1, 0.0, 0.0, 0.0, 'Lechia Gdansk'),
(15, 'Piast Gliwice', 'Lech Poznan', '0', '0', '2019-04-20', '14:14:00', 1, 1, 0.0, 0.0, 0.0, 'remis'),
(17, 'Legia Warszawa', 'Lech Poznan', '1', '3', '2019-05-19', '19:00:00', 1, 1, 0.0, 0.0, 0.0, 'Lech Poznan'),
(20, 'Arka Gdynia', 'Legia Warszawa', '1', '2', '2019-05-08', '11:11:00', 1, 1, 0.0, 0.0, 0.0, 'Legia Warszawa'),
(21, 'Legia Warszawa', 'Lech Poznan', '3', '1', '2019-05-17', '14:44:00', 1, 1, 0.0, 0.0, 0.0, 'Legia Warszawa'),
(22, 'Bayern Monachium', 'Borussia Dortmund', '2', '4', '2019-05-25', '01:01:00', 1, 1, 1.3, 1.4, 1.5, 'Borussia Dortmund'),
(23, 'Borussia M', 'RB Lipsk', '3', '1', '2019-05-24', '12:34:00', 1, 1, 1.6, 2.0, 2.1, 'Borussia M'),
(24, 'VfL Wolfsburg', 'Eintracht Frankfurt', '3', '4', '2019-05-24', '15:15:00', 1, 1, 1.1, 1.4, 1.7, 'Eintracht Frankfurt'),
(25, 'FSV Mainz 05', 'FC Augsburg', '2', '3', '2019-05-31', '06:54:00', 1, 1, 1.2, 1.5, 1.8, 'FC Augsburg'),
(26, 'Hannover 96', 'FC Nuernberg', '2', '3', '2019-05-23', '05:05:00', 1, 1, 1.3, 1.6, 1.9, 'FC Nuernberg'),
(27, 'Eintracht Frankfurt', 'Borussia Dortmund', '2', '1', '2019-05-23', '23:11:00', 1, 1, 1.5, 2.5, 2.7, 'Eintracht Frankfurt'),
(28, 'Borussia M', 'FC Augsburg', '1', '0', '2019-05-31', '13:37:00', 1, 1, 0.0, 0.0, 0.0, 'Borussia M'),
(29, 'VfL Wolfsburg', 'SC Freiburg', '', '', '2019-05-31', '10:10:00', 1, 1, 0.0, 0.0, 0.0, 'remis'),
(30, 'Bayern Monachium', 'Hannover 96', '3', '2', '2019-05-31', '16:16:00', 1, 1, 2.5, 6.4, 1.7, 'Bayern Monachium'),
(31, 'RB Lipsk', 'Fortuna Dusseldorf', '1', '1', '2019-05-31', '20:14:00', 1, 1, 1.5, 1.6, 2.1, 'remis'),
(32, 'Bayern Leverkusen', 'Werder Brema', '', '', '2019-06-01', '11:11:00', 1, 1, 4.3, 3.4, 1.4, 'remis'),
(33, 'Bayern Monachium', 'VfL Wolfsburg', '1', '0', '2019-05-29', '14:44:00', 1, 1, 1.7, 4.6, 2.3, 'Bayern Monachium'),
(34, 'VfL Wolfsburg', 'Bayern Monachium', '2', '1', '2019-05-31', '15:15:00', 1, 1, 4.6, 1.7, 2.3, 'VfL Wolfsburg'),
(35, 'Bayern Monachium', 'Borussia Dortmund', '4', '5', '2019-05-29', '14:14:00', 1, 1, 2.0, 2.2, 2.0, 'remis'),
(36, 'Bayern Monachium', 'Hannover 96', '1', '1', '2019-05-17', '23:02:00', 1, 1, 1.4, 7.7, 3.1, 'Bayern Monachium'),
(37, 'Borussia Dortmund', 'VfB Stuttgard', '1', '0', '2019-06-15', '02:22:00', 1, 1, 1.5, 7.0, 3.0, 'Borussia Dortmund'),
(38, 'Bayern Monachium', 'FC Augsburg', '3', '0', '2019-06-21', '23:33:00', 1, 1, 1.5, 7.0, 3.0, 'Bayern Monachium'),
(39, 'Bayern Leverkusen', 'Hannover 96', '0', '1', '2019-06-07', '11:01:00', 1, 1, 1.6, 5.5, 2.8, 'Hannover 96'),
(40, 'RB Lipsk', 'Werder Brema', '3', '2', '2019-06-19', '20:00:00', 1, 1, 1.8, 3.6, 2.7, 'RB Lipsk'),
(41, 'Bayern Monachium', 'TSG Hoffenheim', '1', '2', '2019-06-14', '11:11:00', 1, 1, 1.6, 5.7, 2.9, 'TSG Hoffenheim'),
(42, 'TSG Hoffenheim', 'FC Nuernberg', '0', '1', '2019-06-07', '22:22:00', 1, 1, 1.7, 4.9, 2.8, 'FC Nuernberg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `obstawione_mecze`
--

CREATE TABLE `obstawione_mecze` (
  `id` int(11) NOT NULL,
  `id_kuponu` int(11) NOT NULL,
  `id_meczu` int(11) NOT NULL,
  `wybor` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `obstawione_mecze`
--

INSERT INTO `obstawione_mecze` (`id`, `id_kuponu`, `id_meczu`, `wybor`) VALUES
(1, 1, 9, 'remis'),
(2, 1, 8, 'Lechia Gdansk'),
(3, 1, 7, 'Lech Poznan'),
(4, 1, 6, 'remis'),
(5, 2, 11, 'remis'),
(6, 2, 15, 'Lech Poznan'),
(7, 3, 12, 'remis'),
(8, 4, 9, 'Piast Gliwice'),
(9, 4, 6, 'Legia Warszawa'),
(10, 5, 8, 'remis'),
(11, 5, 11, 'Gornik Zabrze'),
(12, 5, 15, 'Lech Poznan'),
(13, 6, 8, 'Lechia Gdansk'),
(14, 6, 7, 'Piast Gliwice'),
(15, 6, 6, 'remis'),
(16, 7, 8, 'Arka Gdynia'),
(17, 7, 12, 'Legia Warszawa'),
(18, 7, 11, 'Wisla Krakow'),
(19, 8, 9, 'Piast Gliwice'),
(20, 8, 8, 'remis'),
(21, 8, 7, 'Piast Gliwice'),
(22, 9, 9, 'Piast Gliwice'),
(23, 9, 8, 'remis'),
(24, 10, 7, 'Lech Poznan'),
(25, 11, 7, 'Lech Poznan'),
(26, 12, 7, 'Lech Poznan'),
(27, 12, 6, 'remis'),
(28, 13, 2, 'Piast Gliwice'),
(29, 13, 19, 'remis'),
(30, 12, 20, 'Legia Warszawa'),
(31, 14, 20, 'Legia Warszawa'),
(32, 15, 8, 'Arka Gdynia'),
(33, 17, 8, 'Lechia Gdansk'),
(34, 18, 8, 'remis'),
(35, 19, 23, 'RB Lipsk'),
(36, 19, 22, 'remis'),
(37, 20, 23, 'Borussia M'),
(38, 20, 22, 'Bayern Monachium'),
(39, 22, 23, 'remis'),
(40, 22, 22, 'Borussia Dortmund'),
(41, 22, 26, 'FC Nuernberg'),
(42, 22, 24, 'Eintracht Frankfurt'),
(43, 22, 25, 'FC Augsburg'),
(44, 22, 26, 'FC Nuernberg'),
(45, 22, 24, 'Eintracht Frankfurt'),
(46, 22, 25, 'FC Augsburg'),
(47, 23, 26, 'FC Nuernberg'),
(48, 23, 24, 'Eintracht Frankfurt'),
(49, 23, 25, 'FC Augsburg'),
(50, 24, 26, 'FC Nuernberg'),
(51, 24, 24, 'Eintracht Frankfurt'),
(52, 24, 25, 'remis'),
(53, 25, 26, 'FC Nuernberg'),
(54, 25, 24, 'Eintracht Frankfurt'),
(55, 26, 26, 'Hannover 96'),
(56, 26, 24, 'Eintracht Frankfurt'),
(57, 27, 26, 'FC Nuernberg'),
(58, 28, 26, 'FC Nuernberg'),
(59, 29, 26, 'FC Nuernberg'),
(60, 30, 27, 'remis'),
(61, 32, 27, 'Eintracht Frankfurt'),
(62, 33, 27, 'Eintracht Frankfurt'),
(63, 34, 27, 'remis'),
(64, 35, 27, 'Eintracht Frankfurt'),
(65, 36, 27, 'Borussia Dortmund'),
(66, 37, 28, 'FC Augsburg'),
(67, 38, 28, 'FC Augsburg'),
(68, 39, 32, 'Werder Brema'),
(69, 40, 30, 'Bayern Monachium'),
(70, 40, 31, 'remis'),
(71, 41, 32, 'remis'),
(72, 42, 28, 'remis'),
(73, 43, 32, 'Bayern Leverkusen'),
(74, 44, 32, 'Werder Brema'),
(75, 45, 32, 'Bayern Leverkusen'),
(76, 46, 32, 'Werder Brema'),
(77, 47, 32, 'Bayern Leverkusen'),
(78, 48, 32, 'remis'),
(79, 49, 32, 'Werder Brema'),
(80, 50, 32, 'remis'),
(81, 51, 32, 'Werder Brema'),
(82, 52, 32, 'Werder Brema'),
(83, 53, 32, 'Werder Brema'),
(84, 54, 32, 'Werder Brema'),
(85, 55, 32, 'Werder Brema'),
(86, 56, 32, 'Werder Brema'),
(87, 57, 32, 'Werder Brema'),
(88, 58, 32, 'Werder Brema'),
(89, 59, 32, 'Werder Brema'),
(90, 60, 32, 'Werder Brema'),
(91, 61, 32, 'Werder Brema'),
(92, 62, 32, 'Werder Brema'),
(93, 63, 32, 'remis'),
(94, 64, 32, 'remis'),
(95, 65, 32, 'remis'),
(96, 66, 32, 'remis'),
(97, 67, 32, 'remis'),
(98, 68, 29, 'VfL Wolfsburg'),
(99, 69, 33, 'Bayern Monachium'),
(100, 69, 34, 'Bayern Monachium'),
(101, 69, 32, 'Werder Brema'),
(102, 70, 33, 'Bayern Monachium'),
(103, 70, 28, 'FC Augsburg'),
(104, 71, 39, 'Bayern Leverkusen'),
(105, 71, 42, 'FC Nuernberg'),
(106, 72, 39, 'Hannover 96'),
(107, 72, 42, 'FC Nuernberg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tabela`
--

CREATE TABLE `tabela` (
  `id` int(11) NOT NULL,
  `druzyna` text NOT NULL,
  `liczba_meczow` int(2) NOT NULL,
  `bramki_zdobyte` int(3) NOT NULL,
  `bramki_stracone` int(3) NOT NULL,
  `suma_punktow` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `tabela`
--

INSERT INTO `tabela` (`id`, `druzyna`, `liczba_meczow`, `bramki_zdobyte`, `bramki_stracone`, `suma_punktow`) VALUES
(1, 'Bayern Monachium', 40, 97, 43, 88),
(2, 'Borussia Dortmund', 36, 85, 48, 80),
(3, 'RB Lipsk', 36, 67, 33, 70),
(4, 'Borussia M\'Gladbach', 33, 55, 44, 55),
(5, 'Bayern Leverkusen', 34, 64, 52, 56),
(6, 'Eintracht Frankfurt', 35, 65, 47, 60),
(7, 'VfL Wolfsburg', 36, 59, 55, 56),
(8, 'TSG Hoffenheim', 35, 70, 50, 54),
(9, 'Werder Brema', 34, 58, 51, 51),
(10, 'Hertha Berlin', 33, 48, 52, 43),
(11, 'Fortuna Dusseldorf', 34, 48, 65, 42),
(12, 'FSV Mainz 05', 34, 44, 58, 40),
(13, 'SC Freiburg', 33, 41, 60, 34),
(14, 'FC Augsburg', 36, 53, 69, 35),
(15, 'Schalke 04', 33, 37, 55, 32),
(16, 'VfB Stuttgard', 34, 32, 71, 27),
(17, 'Hannover 96', 37, 37, 78, 24),
(18, 'FC Nuernberg', 35, 29, 65, 25);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `birth` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `konto` double(10,2) NOT NULL DEFAULT '20.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `birth`, `email`, `konto`) VALUES
(1, 'admin', '$2y$10$ebImF3tOYZaBhZgw/SmaD.fUM3Hpvhn0xc2ElALs7wAW5agaFoXmC', '2019-05-17 22:28:38', '1990-05-18', 'admin@admin.pl', 900.74),
(2, 'cos', '$2y$10$B/rlLzpD8Z9wjYK77a2MBuaQ3qpZY8sObPTBVcKKOFdCIlUSqUQrS', '2019-04-10 13:07:57', '1995-03-02', 'cos@cos.pl', 324.00),
(3, 'sad', '$2y$10$YXP/WAEd7JJ2XvOHTADNuuFptVMSExJ67GWtlAwFgCl5b4A2uqQ02', '2019-04-10 14:54:25', '2019-04-24', '123@123.pl', 20.00),
(4, 'asdf', '$2y$10$8gJQpUubVa47SQyZEMRFset4gWNGq2WmjNrPFoXdR.VNESPWjBt.y', '2019-05-15 14:25:39', '2019-05-14', 'asdf@o2.pl', 20.00),
(5, 'qwerty', '$2y$10$ngQl7h8Qc.bqfXM6ZlJ/9ugLWwsJy9tSxXQfrqYV8ZoDks8oq0hRu', '2019-05-29 13:59:56', '1996-01-01', 'qwe@qwe.pl', 20.00),
(6, 'danyl', '$2y$10$9I7IHVW/I3.SI3rmWC37cehaJjenNOJtEoakTQyVI.lyv/7hgh0jS', '2019-05-29 14:05:29', '1998-04-22', 'danyl@asdsds.pl', 20.00),
(7, 'Damjan', '$2y$10$5bOj0zee70uJLq6ZRPlepOOifUFh.MBDPAF7GVaiwfUuVEgrT/wJ.', '2019-06-05 15:21:49', '1990-04-03', 'damian@damian.pl', 20.00);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kupony`
--
ALTER TABLE `kupony`
  ADD PRIMARY KEY (`id_kuponu`);

--
-- Indeksy dla tabeli `mecze`
--
ALTER TABLE `mecze`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `obstawione_mecze`
--
ALTER TABLE `obstawione_mecze`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `tabela`
--
ALTER TABLE `tabela`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kupony`
--
ALTER TABLE `kupony`
  MODIFY `id_kuponu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT dla tabeli `mecze`
--
ALTER TABLE `mecze`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT dla tabeli `obstawione_mecze`
--
ALTER TABLE `obstawione_mecze`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT dla tabeli `tabela`
--
ALTER TABLE `tabela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
