<?php
	include "navbar-menu.php";
	echo "<header class='masthead'>";
		$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
		if($db===false){
			die("Error:".mysqli_connect_error());
		}
		$new_mail = $confim_mail = "";
		$mail_err = $confim_err = $komunikat = "";
		
		if($_SERVER["REQUEST_METHOD"] == "POST"){
			
			if(empty(trim($_POST["new"])))
				$mail_err = "Podaj mail!";
			else{
				$sql = "SELECT id FROM users WHERE email = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_email);
					
					$param_email = trim($_POST["new"]);
							
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						if(mysqli_stmt_num_rows($stmt) == 1){
							$mail_err = "Email zajęty";
						} else{
							$new_mail = trim($_POST["new"]);
						}
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
			
			if(empty(trim($_POST["confim"])))
				$confim_err = "Podaj mail!";
			else{ 
				$confim_mail = trim($_POST["confim"]);
				if(empty($mail_err) && $new_mail != $confim_mail)
				$confim_err = "Maile różnią się!";
			}
			
			if(empty($mail_err) && empty($confim_err)){
			
			
				$zapyt = "UPDATE users SET email = ? WHERE username = ?";
					if($stmt = mysqli_prepare($db,$zapyt)){
						mysqli_stmt_bind_param($stmt,"ss",$param_mail,$param_username);
						$param_mail = $new_mail;
						$param_username = $_SESSION["username"];
									
						if(mysqli_stmt_execute($stmt))
							$komunikat = "Zmieniono pomyślnie!";
						else
							$komunikat = "Błąd!";
					}	
			}	
							
		}	
			
	?>
	<h3 class='text-white row align-items-center justify-content-center'>Zmień mail</h3>
	<div class="row align-items-center justify-content-center" style="margin-top: 10px;">
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
	
	<div class="input-group mb-4">
    <div class="input-group-prepend">
	<span class="input-group-text " id="basic-addon1" style="width: 200px;">Podaj nowy adres email:</span>
	</div>
	<input type="email" style="width: 230px;" class="form-control" name="new" placeholder="<?php echo $mail_err; ?>">
	</div>
	
	
	<div class="input-group mb-4">
    <div class="input-group-prepend">
	<span class="input-group-text " id="basic-addon1" style="width: 200px;">Powtórz adres email:</span>
	</div>
	<input type="email" class="form-control" name="confim" style="width: 230px;" placeholder="<?php echo $confim_err; ?>">
	</div>
	
	
	<div class="row align-items-center justify-content-center">
	<button type="sumbit" value="Zmień" class="btn btn-primary btn-xl js-scroll-trigger">Zmień</button>
	</div>
	<span><?php echo $komunikat; ?></span>
	
	
	</div>
	</form>
	
	<?php 
		echo "</header>";
		include "footer.php";
	?>
