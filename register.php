<?php
		
			$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
				if($db===false){
					die("Error:".mysqli_connect_error());
				}
			$username = $password = $confim_password = $email = $birth = "";
			$username_err = $password_err = $confirm_password_err = $email_err = $birth_err = "";
			
			if($_SERVER["REQUEST_METHOD"] == "POST"){
 
			
			if(empty(trim($_POST["username"]))){
				$username_err = "Podaj nazwę użytkownika!.";
			} else{
				
				$sql = "SELECT id FROM users WHERE username = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_username);
					
					$param_username = trim($_POST["username"]);
								
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						if(mysqli_stmt_num_rows($stmt) == 1){
							$username_err = "Nazwa użytkownika zajęta.";
						} else{
							$username = trim($_POST["username"]);
						}
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
		
			if(empty(trim($_POST["password"]))){
				$password_err = "Podaj hasło.";     
			} elseif(strlen(trim($_POST["password"])) < 6){
				$password_err = "Hasło musi posiadać conajmniej 6 znaków!.";
			} else{
				$password = trim($_POST["password"]);
			}
			
			if(empty(trim($_POST["confirm_password"]))){
				$confirm_password_err = "Podaj ponownie hasło.";     
			} else{
				$confirm_password = trim($_POST["confirm_password"]);
				if(empty($password_err) && ($password != $confirm_password)){
					$confirm_password_err = "Hasła różnią się.";
				}
			}
			
			if(empty(trim($_POST["email"]))){
				$email_err = "Podaj email!.";
			} else{
				
				$sql = "SELECT id FROM users WHERE email = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_email);
					
					$param_email = trim($_POST["email"]);
							
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						if(mysqli_stmt_num_rows($stmt) == 1){
							$email_err = "Email zajęty.";
						} else{
							$email = trim($_POST["email"]);
						}
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
		
		$data_dzis = strtotime(date("Y-m-d"));
		$data = (strtotime($_POST["birth"])) ;
		
		if(empty(($_POST["birth"])))
				$birth_err = "<font class='text-warning'><b>Podaj datę urodzenia!</b></font>";
		else if((($data_dzis - $data)/31536000)<18)
				$birth_err = "<font class='text-warning'><b>Jesteś niepełnoletni!</b></font>";
				
			
		if(empty($birth_err))
		{
				
				$sql = "SELECT id FROM users WHERE birth = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_birth);
					
					$param_birth = $_POST["birth"];
						
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						$birth = $_POST["birth"];
						
						
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
			
			if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($email_err) && empty($birth_err)){
				
				$sql = "INSERT INTO users (username, password, birth, email) VALUES (?, ?, ?, ?)";
				 
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "ssss", $param_username, $param_password, $param_birth, $param_email);
					
					$param_username = $username;
					$param_password = password_hash($password, PASSWORD_DEFAULT);
					$param_birth= $birth;
					$param_email = $email;	
					
					if(mysqli_stmt_execute($stmt)){
						header("location: login.php");
					} else{
						echo "Błąd... Spróbuj jeszcze raz";
					}
				}	
				mysqli_stmt_close($stmt);
			}
			mysqli_close($db);
		}
		
		
		
		?>
<!DOCTYPE html>
<html lang="pl">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Zakłady Typowy Typer</title>

  <!-- Font Awesome Icons -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index1.html">Zakłady Typowy Typer</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#page-top">Załóż konto</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="login.php">Zaloguj się</a>
			
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <header class="masthead">
				<div>
				  <div class="text-white-75 font-weight-light mb-5">
                 <center>
				<h2>Zarejestruj się!</h2>
				<br>
				<div class="row align-items-center justify-content-center">
				<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
					<div  <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>>
					<div class="input-group mb-4">
                    <div class="input-group-prepend">
					
						<span class="input-group-text" id="basic-addon1" style="width: 200px;">Nazwa użytkownika : </span>
						</div>
						<input type="text" class="form-control" style="width: 230px;" name="username" placeholder="<?php echo $username_err; ?>" value="<?php echo $username; ?>">
						</div>
					</div>  


					
					<div  <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>>
						<div class="input-group mb-4">
                         <div class="input-group-prepend">
			
                        <span class="input-group-text" id="basic-addon1" style="width: 200px;" >Podaj hasło :</span>
                        </div>
                        <input type="password" class="form-control" style="width: 230px;" placeholder="<?php echo $password_err; ?>" name="password" >
                        
                        </div>
						
						<div  <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>>
					     <div class="input-group mb-4">
                         <div class="input-group-prepend">
						 <span class="input-group-text" id="basic-addon1" style="width: 200px;" >Potwierdź hasło :</span>
						</div>
						<input type="password" class="form-control" style="width: 230px;" placeholder="<?php echo $confirm_password_err; ?>" name="confirm_password">
						
					</div>
            
					<div   <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>>
					     <div class="input-group mb-4">
                         <div class="input-group-prepend">
						 <span class="input-group-text" id="basic-addon1" style="width: 200px;" >Podaj adres email:</span>
						</div>
						<input type="email" class="form-control" name="email" placeholder="<?php echo $email_err; ?>" value="<?php echo $email; ?>">
					</div>

	
					
		
	
					<div   <?php echo (!empty($birth_err)) ? 'has-error' : ''; ?>>
					     <div class="input-group mb-4">
                         <div class="input-group-prepend">
						 <span class="input-group-text" id="basic-addon1" style="width: 200px;">Podaj datę urodzenia :</span>
						</div>
						<input type="date" class="form-control" name="birth" value="<?php echo $birth; ?>">
						
					</div>
					<span><?php echo $birth_err; ?></span>
					
					<div>
						<button type="submit"  value="Zarejestruj" class="btn btn-primary btn-xl js-scroll-trigger">Zarejestruj</button>
					</div>
					<br><br><br><p>Masz już konto? <a href="login.php">Zaloguj się tutaj!</a></p>
					</center>
				</form>
				</div>  
</header>
<footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">Copyright &copy; 2019 - Zakłady Typowy Typer</div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/creative.min.js"></script>				
</body>
</html>